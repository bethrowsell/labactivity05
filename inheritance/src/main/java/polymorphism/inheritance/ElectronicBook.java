package polymorphism.inheritance;

public class ElectronicBook extends Book{
    private int numberBytes;

    public ElectronicBook(String t, String a, int n){
        super(t, a);
        this.numberBytes = n;
    }

    public String toString(){
        String s = super.toString();
        return s + ", Number of bytes: " + this.numberBytes;
    }

    public int getNumberBytes(){
        return this.numberBytes;
    }
}
