package polymorphism.inheritance;

public class BookStore {
    
    public static void main(String[] args){
            Book[] book = new Book[5];
            book[0] = new Book("Twilight", "Stephanie Meyers");
            book[1] = new ElectronicBook("The Hunger Games", "Suzanne Collins", 1800000);
            book[2] = new Book("A Summer To Die", "Lois Lowry");
            book[3] = new ElectronicBook("Eragon", "Christopher Paolini", 1200000);
            book[4] = new ElectronicBook("The Hobbit", "J.R.R Tolkien", 600000);
   
            for(int i = 0; i < book.length; i++){
                System.out.println(book[i].toString());
            }
    }
}
