package polymorphism.inheritance;

public class Book {
    protected String title;
    private String author;

    public Book(String t, String a){
        this.title = t;
        this.author = a;
    }

    public String toString(){
        String s = "";
        return s + "Title: " + this.title + ", Author: " + this.author;
    }

    public String getTitle(){
        return this.title;
    }

    public String getAuthor(){
        return this.author;
    }
}
